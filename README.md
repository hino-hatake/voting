Voting App from Dockersamples
=========

A simple distributed application running across multiple Docker containers.

Sauce: https://github.com/dockersamples/example-voting-app

---------------

## Getting started

The Linux stack uses `Python`, `Node.js`, `Java`, with `Redis` for messaging and `Postgres` for storage.

Run in this directory:
```bash
docker-compose up -d && docker-compose logs -f
```

The app will be running at [http://localhost:5000](http://localhost:5000), and the results will be at [http://localhost:5001](http://localhost:5001).

## Architecture

![Architecture diagram](architecture.png)

* A front-end web app in [Python](/vote) which lets you vote between two options
* A [Redis](https://hub.docker.com/_/redis/) or [NATS](https://hub.docker.com/_/nats/) queue which collects new votes
* A [Java](/worker/src/main) worker which consumes votes and stores them in...
* A [Postgres](https://hub.docker.com/_/postgres/) or [TiDB](https://hub.docker.com/r/dockersamples/tidb/tags/) database backed by a Docker volume
* A [Node.js](/result) webapp which shows the results of the voting in real time

## Notes

#### Cookies

The voting application only accepts one vote per client. It does not register votes if a vote has already been submitted from a client. You could _remove/block your browser cookies data_ to hack the vote :frog:.

#### Seeding

Check [seed-data/](seed-data/) for details, below is how to run it:
```bash
docker-compose -f compose.seed.yml run --rm seed
```

#### Testing

Inside [result/](result/), there're testing thingy to failsafe your code changes, just `cd` into and:
```bash
docker-compose up -d && docker-compose logs -f
```
